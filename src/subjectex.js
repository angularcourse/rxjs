import { Subject, AsyncSubject } from 'rxjs'


// The Observer (x object) who is subscribing to the observable (Subject) should be of that type only
let subjectObservable = new Subject()

let xObserver = {
    next: (o) => console.log(`From Subject Next : ${o}`),
    error: (o) => console.log(`From Subject Error : ${o}`),
    complete: (o) => console.log(`From Subject Compelte`),
}
subjectObservable.subscribe(xObserver)
subjectObservable.next(1);
subjectObservable.next(2);
subjectObservable.next(3);

subjectObservable.complete();


// subjectObservable.next('Hi X Observer , I am Subject Emitting the event ')
subjectObservable.error(new Error('Hi X Observer , I am Subject returning 404 (Not Found)'));
    

// Return the value only after complete() fn's execution
let asyncSubj = new AsyncSubject();
let yObserver = {
    next: (o) => console.log(`From Async Subject Next : ${o}`),
    error: (o) => console.log(`From Async Subject Error : ${o}`),
    complete: (o) => console.log(`From Async Subject Complete` ),
}


asyncSubj.next(1);
asyncSubj.next(2);
asyncSubj.next(3);

asyncSubj.complete();

asyncSubj.subscribe(yObserver)
asyncSubj.unsubscribe()
