import { map, delay, switchAll, switchMap } from 'rxjs/operators';
import { BehaviorSubject, from, merge, of }  from 'rxjs';
import { mergeAll } from 'rxjs/operators';

// fetch data from server 

// Mergemap = MergeAll + Map, all the inner obcervables will be handled by MergeAll



let getDataFromServer = (args) => {
    return of(`fetched data from server ${args}`)
    .pipe(
        delay(2000)
    )
}

// from(['a','n','g','u','l','a','r']).pipe(
//     map( x =>  getDataFromServer(x)),
// ).subscribe( val => val.subscribe( data =>  console.log(data)))



// from(['a','n','g','u','l','a','r']).pipe(
//     map( x =>  getDataFromServer(x)),
//     mergeAll()
// ).subscribe( val => console.log(val))


from(['a','n','g','u','l','a','r']).pipe(
    map( x =>  getDataFromServer(x)),
    switchAll()
).subscribe( val => console.log(val))


// switchMap = switchAll + map
from(['a','n','g','u','l','a','r']).pipe(
    switchMap( x =>  getDataFromServer(x)),
).subscribe( val => console.log(val))


// of , from , fetch
