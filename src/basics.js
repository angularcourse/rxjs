import { map, reduce, filter } from 'rxjs/operators'
import { of } from 'rxjs'


map( x => x * 2 )
(of(8,9,10)).subscribe( (res) => {
    console.log(res);
})  


let values = of(67,87,45,54,102, 105, 876)

values.pipe(
    map( x=> x*4 )
).subscribe( res => console.log('res1', res));


values.pipe(
    filter( x=> x%2 === 0 )
).subscribe( res => console.log('filtered values', res));


// acc = 0 
// acc += 67 
// acc += 87 
// acc += 87 
// acc += 87 


values.pipe(
    reduce( (acc, x) => acc+x , 10 )
).subscribe( res => console.log('reduced value', res));


values.pipe(
    filter( x=> x%2 === 0 ),
    reduce( (acc, x) => acc+x , 0 )
).subscribe( res => console.log('reduced value = sum of only even numbers', res));
