
//itinerary=DEL-BLR-28/01/2021&tripType=O&paxType=A-1_C-0_I-0&intl=false&cabinClass=E&ccde=IN&lang=eng

import { BehaviorSubject, from, of } from "rxjs";
import { concatMap, delay, map, mergeMap, switchMap } from "rxjs/operators";


let getDataFromServer = (args) => {
    let dt = Math.floor((Math.random() + 1000))+1;
    return of(`fetched data from server ${args}: ${dt} secs`)
    .pipe(
        delay(dt)
    )
}


const filtersArr = ['itinerary=DEL-BLR-28/01/2021', 'tripType=O', 'paxType=A-1_C-0_I-0', 'cabinClass=E', 'ccde=IN']
let appliedFilter$ = new BehaviorSubject('');

let applyFilter = () => {
    filtersArr.forEach( (filter, index) => {
                let  newFilter = appliedFilter$.value
            if(index == 0)
            newFilter = `?${filter}`
            else
            newFilter = `${newFilter}&${filter}`
          
            appliedFilter$.next(newFilter)
    } )
}


// on tap of search ... button 
applyFilter()

// appliedFilter$.pipe(
//     map( x=> getDataFromServer(x))
// ).subscribe( res => res.subscribe( value => console.log(value)))


// appliedFilter$.pipe(
//     switchMap( x=> getDataFromServer(x))
// ).subscribe( res => console.log(res))

// // concatMap lets first observable to complete before subscribing to the next

// appliedFilter$.pipe(
//     concatMap( x => getDataFromServer(x))
// ).subscribe( res => console.log('concatMap', res))


//------------

console.log('----concatMap-----')
from([2,3,4,5,6]).pipe(
    concatMap( x => getDataFromServer(x))
).subscribe( res => console.log('concatMap', res))


console.log('----mergeMap-----')
from([2,3,4,5,6]).pipe(
    mergeMap( x => getDataFromServer(x))
).subscribe( res => console.log('mergeMap', res))

console.log('----map-----')
from([2,3,4,5,6]).pipe(
    map( x => getDataFromServer(x))
    ).subscribe( res => res.subscribe( value => console.log('MAP', value)))

let p = [2,3,4,5,6];
for(let i of p){
    console.log(i)
}

let filter = {
    "0": 'Mumbai',
    "1": "Pune",
    length: 2
}

for(let i in filter){
    console.log(i)
}