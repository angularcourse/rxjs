import { combineLatest, of} from 'rxjs'
import {combineAll, map } from 'rxjs/operators'

// combineLatest([of(8,9,10), of(67,875, 876), of(67,87,45,54,102, 105, 876)])
// .subscribe( ([v1, v2, v3 ]) => {
//     console.log('v1', v1);
//     console.log('v2', v2);
//     console.log('v3', v3);
// });



let ob1$ =  of(8,9,10).pipe()
let ob2$ = ob1$.pipe(
    map( v =>  of(67,875, 876).pipe(
        map ( fv => `Output will be ${v}: ${fv}`)
    ) )
)

ob2$.pipe(combineAll()).subscribe(console.log);
