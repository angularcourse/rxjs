import { combineAll, map, sample, take } from 'rxjs/operators';
import {interval} from 'rxjs';

let observ$ = interval(2000).pipe( 
    take(6)
)

// observ$ will emit the value of 6 after every 2 seconds

let sample$ = observ$.pipe( 
    map(x => 
        interval(2000).pipe(
            map( n => `Value of n ${x} : ${n}`),
            take(9)
        )
)
);

sample$.pipe(combineAll())
.subscribe(console.log)