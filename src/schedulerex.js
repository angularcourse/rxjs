import { Observable, Subscriber, asyncScheduler } from "rxjs";
import { observeOn } from 'rxjs/operators';
let observableObj = new Observable( (subscriber) => {
        subscriber.next('X1')
        subscriber.next('X2')
        subscriber.next('X3')
        subscriber.next('X4')
        subscriber.next('X5')
        subscriber.next('X6')
        subscriber.next('X7')
        subscriber.next('X8')
        subscriber.complete()
}).pipe(
    observeOn(asyncScheduler, 2000)
)

console.log(' we have overservable ');


let finalObserver = {
    next(x) {  console.log('x',x) },
    error(err) { console.log('error', err)},
    comlete() {console.log('compeleted')}
}

let intermediateObserver = {
    next(val) {
       asyncScheduler.schedule(
           (x) => finalObserver.next(x),
           0,
           val 
       )
    }
}


observableObj.subscribe(finalObserver);





// observableObj.subscribe( x => console.log(x), 
// (error) => console.log(error),
// () => console.log(' Completed '))



